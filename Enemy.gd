extends Area2D


enum Location {
	Top = 1,
	Left = 1,
	Bottom = 2,
	Right = 3
}

export var min_speed = 1.0
export var max_speed = 5.0
var lower_bounds
var upper_bounds
var speed

func _ready():
	add_to_group("entities")
	add_to_group("enemies")
	speed = min_speed + (randf() * max_speed)
	
func _process(_delta):
	if position.x <= lower_bounds.x:
		 rotation_degrees = get_random_direction(Location.Top)
	if position.y <= lower_bounds.y:
		 rotation_degrees = get_random_direction(Location.Left)
	if position.y >= upper_bounds.y:
		 rotation_degrees = get_random_direction(Location.Bottom)
	if position.x >= upper_bounds.x:
		 rotation_degrees = get_random_direction(Location.Right)
		
	speed = min_speed + (randf() * max_speed)
	var radians = deg2rad(rotation_degrees)
	position.x += cos(radians) * speed
	position.y += sin(radians) * speed
	position.x = clamp(position.x, lower_bounds.x, upper_bounds.x)
	position.y = clamp(position.y, lower_bounds.y, upper_bounds.y)


func get_random_direction(direction):
	if direction == Location.Top: #top
		return (randf() * 180)
	if direction == Location.Left: #left
		return (randf() * 90) if(randf() < 0.5) else rotation + 270
	if direction == Location.Bottom: #bottom
		return (randf() * 180) + 180
	if direction == Location.Right: #right
		return (randf() * 180) + 90
		
