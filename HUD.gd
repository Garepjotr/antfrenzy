extends CanvasLayer

signal start_game


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_StartButton_pressed():
	$StartButton.hide()
	emit_signal("start_game")
	
func show():
	var children = get_children()
	for child in children:
		child.show()

func hide():
	var children = get_children()
	for child in children:
		child.hide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
