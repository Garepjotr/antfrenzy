extends Node2D

export (PackedScene) var Enemy
export (PackedScene) var Player
export (PackedScene) var Food

export var staring_enemies = 0
export var staring_food = 500
var lower_bounds
var upper_bounds
var score = 0

func _ready():
	var playfield_position = $Bounds.position
	var playfield_extents = $Bounds.get_shape().get_extents()
	lower_bounds = Vector2(playfield_position.x - playfield_extents.x, playfield_position.y - playfield_extents.y)
	upper_bounds = Vector2(playfield_position.x + playfield_extents.x, playfield_position.y + playfield_extents.y)

	$HUD.show()

func _on_Player_hit():
	get_tree().call_group("entities","queue_free")
	$HUD.show()

func _on_Player_nom():
	score += 1
	spawn_enemy()
	spawn_food()
	$HUD/Score.set_text("Your score: %d" % [score])

func _on_HUD_start_game():
	randomize()
	score = 0
	
	var player = Player.instance()
	player.position = $PlayerStart.position
	player.upper_bounds = upper_bounds
	player.lower_bounds = lower_bounds
	add_child(player)
	player.connect("nom", self, "_on_Player_nom")
	player.connect("hit", self, "_on_Player_hit")
	
	for _n in range(staring_enemies):
		spawn_enemy()
	
	for _n in range(staring_food):
		spawn_food()
	
	$HUD.hide()

func spawn_enemy():
	$EnemySpawner/EnemySpawnLocation.offset = randi()
	var enemy = Enemy.instance()
	var direction = $EnemySpawner/EnemySpawnLocation.rotation + PI / 2
	enemy.position = $EnemySpawner/EnemySpawnLocation.position
	direction += rand_range(-PI / 4, PI / 4)
	enemy.rotation = direction
	enemy.upper_bounds = upper_bounds
	enemy.lower_bounds = lower_bounds
	call_deferred("add_child", enemy)
	
func spawn_food():
	var food = Food.instance()
	food.position.x = lower_bounds.x + randf() * upper_bounds.x - lower_bounds.x
	food.position.y = lower_bounds.y + randf() * upper_bounds.y - lower_bounds.y
	call_deferred("add_child", food)
