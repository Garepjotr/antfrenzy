extends Area2D

signal hit
signal nom

export var min_speed = 3.0
export var max_speed = 6.0
export var turn_rate = 300.0
export var acceleration = 2.0
var lower_bounds
var upper_bounds
var speed = 0.0

func _ready():
	add_to_group("entities")
	position.x = clamp(position.x, lower_bounds.x, upper_bounds.x)
	position.y = clamp(position.y, lower_bounds.y, upper_bounds.y)

func _process(delta):
	if Input.is_action_pressed("ui_right"):
		rotation_degrees += turn_rate * delta
		rotation_degrees = rotation_degrees if rotation_degrees < 360.0 else 0.0
	if Input.is_action_pressed("ui_left"):
		rotation_degrees -= turn_rate * delta
		rotation_degrees = rotation_degrees if rotation_degrees > 0 else 360.0
	if Input.is_action_pressed("ui_down"):
		speed -= acceleration * delta
	if Input.is_action_pressed("ui_up"):
		speed += acceleration * delta
		
	speed = speed if speed > min_speed else min_speed	
	speed = speed if speed < max_speed else max_speed

	var radians = deg2rad(rotation_degrees)
	position.x += cos(radians) * speed
	position.y += sin(radians) * speed
	position.x = clamp(position.x, lower_bounds.x, upper_bounds.x)
	position.y = clamp(position.y, lower_bounds.y, upper_bounds.y)

func _on_Player_area_shape_entered(_area_id, area, _area_shape, _self_shape):
	if area.is_in_group("enemies"):
		emit_signal("hit")
		$CollisionShape2D.set_deferred("disabled", true)
	elif area.is_in_group("food"):
		emit_signal("nom")
		area.queue_free()
